import argparse
import os
import logging
from logging.config import fileConfig

import xmltodict

from api.product import views
from api.config import logging_file

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
fileConfig(os.path.join(BASE_PATH, logging_file))
logger = logging.getLogger()


def parse_args():

    parser = argparse.ArgumentParser(description='Preprocess data for Contextual Bandit')
    parser.add_argument('-f','--file', help='Input file to be processed', required=True)
    args = vars(parser.parse_args())
    return args


if __name__ == '__main__':
    arg = parse_args()
    filename = arg['file']
    with open(filename, 'r') as f:
        count = 0
        for line in f:
            try:
                if count == 0:
                    count += 1
                    continue
                jsn = xmltodict.parse(line)
                views.parse(jsn)
                count += 1
            except Exception, e:
                logger.debug('Exception occurred: %s (%s) at line: %s ' % (Exception, e, line))
                print Exception, e
                print line
                continue
