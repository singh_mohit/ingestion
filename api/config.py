import os


import ConfigParser


BASE_PATH = os.path.abspath(os.path.dirname(__file__))

config = ConfigParser.ConfigParser()
config.read(os.path.join(BASE_PATH, 'confs/ingestion.cfg.dev'))
mysql_username = config.get('mysql', 'username')
mysql_password = config.get('mysql', 'password')
mysql_host = config.get('mysql', 'host')
mysql_port = config.getint('mysql', 'port')
logging_file = config.get('logging', 'filename')
logging_file = os.path.join(BASE_PATH, 'confs', logging_file)

SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@%s:%d/ingestiondb' % (mysql_username, mysql_password, mysql_host, mysql_port)