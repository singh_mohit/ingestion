import datetime

from api import db


class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True)
    raw_product_id = db.Column(db.BigInteger, index=True)
    sku_id = db.Column(db.Integer, index=True)
    name = db.Column(db.String(512))
    source = db.Column(db.String(25))
    retail_price = db.Column(db.Float)
    sale_price = db.Column(db.Float)
    currency = db.Column(db.String(20))
    date_updated = db.Column(db.DateTime, default=datetime.datetime.now())

    # add one-one relationship info
    brand_id = db.Column(db.Integer, db.ForeignKey('brands.id'))
    manufacturer_id = db.Column(db.Integer, db.ForeignKey('manufacturers.id'))
    shipping_id = db.Column(db.Integer, db.ForeignKey('shippings.id'))
    product_availability_id = db.Column(db.Integer, db.ForeignKey('product_availability.id'))
    product_description_id = db.Column(db.Integer, db.ForeignKey('product_descriptions.id'))
    product_attributes_id = db.Column(db.Integer, db.ForeignKey('product_attributes.id'))

    def __repr__(self):
        return '<id: %s, product_id: %s, sku_id: %s, name: %s, source: %s, retail_price: %s,' \
               'sale_price: %s, currency:%s, brand: %s >' % (self.id, self.raw_product_id,
                                                  self.sku_id, self.name,
                                                  self.source, self.retail_price,
                                                  self.sale_price, self.currency, self.brand_id)


class Brand(db.Model):
    __tablename__ = 'brands'
    id = db.Column(db.Integer, primary_key=True)
    brand_name = db.Column(db.String(50), index=True)
    date_updated = db.Column(db.DateTime,  default=datetime.datetime.now())
    products = db.relationship('Product', backref='brand')

    def __repr__(self):
        return '<brand_id: %s, brand_name: %s>' % (self.id, self.brand_name)


class Manufacturer(db.Model):
    __tablename__ = 'manufacturers'
    id = db.Column(db.Integer, primary_key=True)
    manufacturer_name = db.Column(db.String(100))
    date_updated = db.Column(db.DateTime,  default=datetime.datetime.now())
    products = db.relationship('Product', backref='manufacturer', lazy='dynamic')

    def __repr__(self):
        return '<manufacturer_id: %s, manufacturer_name: %s>' % (self.id, self.manufacturer_name)


class Shipping(db.Model):
    __tablename__ = 'shippings'
    id = db.Column(db.Integer, primary_key=True)
    raw_product_id = db.Column(db.BigInteger, index=True)
    shipping_amount = db.Column(db.Float)
    currency = db.Column(db.String(20))
    date_updated = db.Column(db.DateTime,  default=datetime.datetime.now())
    products = db.relationship('Product', backref='shipping', lazy='dynamic')

    def __repr__(self):
        return '<id: %s, shipping amount: %s, currency: %s>' % (self.id, self.shipping_amount, self.currency)


class ProductAvailability(db.Model):
    __tablename__ = 'product_availability'
    id = db.Column(db.Integer, primary_key=True)
    raw_product_id = db.Column(db.BigInteger, primary_key=True)
    is_available = db.Column(db.String(20))
    date_updated = db.Column(db.DateTime,  default=datetime.datetime.now())
    products = db.relationship('Product', backref='product_availability', lazy='dynamic')

    def __repr__(self):
        return '<id: %s, is_available: %s>' % (self.id, self.is_available)


class ProductAttribute(db.Model):
    __tablename__ = 'product_attributes'
    id = db.Column(db.Integer, primary_key=True)
    raw_product_id = db.Column(db.BigInteger, index=True)
    product_type = db.Column(db.String(70), index=True)
    size = db.Column(db.String(250), index=True)
    material = db.Column(db.String(250), index=True)
    raw_color = db.Column(db.String(250), index=True)
    gender = db.Column(db.String(70), index=True)
    age = db.Column(db.String(70), index=True)
    products = db.relationship('Product', backref='product_attribute', lazy='dynamic')
    date_updated = db.Column(db.DateTime, default=datetime.datetime.now())

    def __repr__(self):
        return '<id: %s, product_type: %s>' % (self.id, self.product_type)


class ProductDescription(db.Model):
    __tablename__ = 'product_descriptions'
    id = db.Column(db.Integer, primary_key=True)
    raw_product_id = db.Column(db.BigInteger, index=True)
    primary_category_id = db.Column(db.String(50))
    secondary_category_id = db.Column(db.String(50), default=None)
    description = db.Column(db.Text)
    description_long = db.Column(db.Text)
    keywords = db.Column(db.Text)
    product_type_id = db.Column(db.Integer)
    date_updated = db.Column(db.DateTime, default=datetime.datetime.now())
    products = db.relationship('Product', backref='product_description', lazy='dynamic')

    def __repr__(self):
        return '<id: %s, description: %s>' % (self.id, self.description)
