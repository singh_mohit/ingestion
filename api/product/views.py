import datetime

from api import db
from api.product import models

NOW = datetime.datetime.now


def _parse(jsn):
    product_info = jsn.get('product', None)
    if not product_info:
        return None
    product_id = product_info.get('@product_id', None)
    product_name = product_info.get('@name', None)
    sku_number = product_info.get('@sku_number', None)
    manufacturer_name = product_info.get('@manufacturer_name', None)
    category = product_info.get('category', None)
    url = product_info.get('URL', None)
    description = product_info.get('description', None)
    discount = product_info.get('discount', None)
    price = product_info.get('price', None)
    brand = product_info.get('brand', None)
    shipping = product_info.get('shipping', None)
    keywords = product_info.get('keywords', None)
    attributes = product_info.get('attributeClass', None)
    return dict(product_id=product_id, product_name=product_name,
                sku_number=sku_number, manufacturer_name=manufacturer_name,
                category=category, url=url, description=description, discount=discount,
                price_details=price, brand_name=brand, shipping=shipping, keywords=keywords,
                attributes=attributes)


def _process_product_records(product_attrs):
    price_details = product_attrs['price_details']
    retail_price = price_details.get('retail_price', None)
    sale_price = price_details.get('sale_price', None)
    if retail_price: retail_price = float(retail_price)
    if sale_price: sale_price = float(sale_price)
    currency = price_details.get('@currency', None)
    product_id = product_attrs['product_id']
    sku_id = product_attrs['sku_number']
    product = models.Product.query.filter_by({'product_id': product_id,
                                              'sku_id': sku_id
                                              }).first()
    if product is None:
        product = models.Product(product_id=product_attrs['product_id'],
                                 sku_id=product_attrs['sku_number'],
                                 source=product_attrs['source'], retail_price=retail_price,
                                 sale_price=sale_price, currency=currency,
                                 )
    else:
        product.retail_price = retail_price
        product.sale_price = sale_price
        product.currency = currency
    db.session.add(product)
    db.session.commit()


def _add(obj):
    db.session.add(obj)
    db.session.commit()


def check_if_exists(model, query):
    obj_exists = model.query.filter_by(**query).first()
    return True if obj_exists else False


def get_obj(model, query):
    obj_exists = model.query.filter_by(**query).first()
    return obj_exists


def parse(jsn, source='rakutten'):
    product_attrs = _parse(jsn)
    product_attrs['source'] = source

    brand_name = product_attrs['brand_name']

    brand_attrs = dict(brand_name=brand_name)
    brand_obj = models.Brand(**brand_attrs)
    brand_filter_query = {'brand_name': brand_name}
    brand_exists = check_if_exists(models.Brand, brand_filter_query)
    if not brand_exists:
        _add(brand_obj)

    manufacturer_name = product_attrs['manufacturer_name']
    manufacturer_attrs = dict(manufacturer_name=manufacturer_name)
    manufacturer_obj = models.Manufacturer(**manufacturer_attrs)
    manufacturer_filter_query = {'manufacturer_name': manufacturer_name}
    manufacturer_exists = check_if_exists(models.Manufacturer, manufacturer_filter_query)

    if not manufacturer_exists:
        _add(manufacturer_obj)

    raw_product_id = product_attrs['product_id']
    shipping_amount = product_attrs['shipping']['cost']['amount']
    shipping_currency = product_attrs['shipping']['cost']['currency']
    shipping_attrs = dict(raw_product_id=raw_product_id,
                          shipping_amount=shipping_amount,
                          currency=shipping_currency)
    shipping_obj = models.Shipping(**shipping_attrs)
    raw_product_id_filter_query = {'raw_product_id': raw_product_id}
    product_shipping_exists = check_if_exists(models.Shipping, raw_product_id_filter_query)
    if product_shipping_exists:
        # update
        shipping_obj = get_obj(models.Shipping, raw_product_id_filter_query)
        shipping_obj.shipping_amount = shipping_amount
        shipping_obj.shipping_currency = shipping_currency
        shipping_obj.date_updated = NOW()
    _add(shipping_obj)

    is_available = product_attrs['shipping'].get('availability', None)
    is_available_attrs = dict(raw_product_id=raw_product_id, is_available=is_available)
    is_available_obj = models.ProductAvailability(**is_available_attrs)
    prod_avail_exists = check_if_exists(models.ProductAvailability, raw_product_id_filter_query)
    if prod_avail_exists:
        # update
        is_available_obj = get_obj(models.ProductAvailability, raw_product_id_filter_query)
        is_available_obj.is_available = is_available
        is_available_obj.date_updated = NOW()
    _add(is_available_obj)

    product_attributes = product_attrs['attributes']
    product_type = product_attributes.get('Product_Type', None)
    size = product_attributes.get('Size', None)
    material = product_attributes.get('Material', None)
    color = product_attributes.get('Color', None)
    gender = product_attributes.get('Gender', None)
    age = product_attributes.get('Age', None)
    product_attributes_attrs = dict(raw_product_id=raw_product_id,
                                    product_type=product_type, size=size,
                                    material=material, raw_color=color,
                                    gender=gender, age=age)
    product_attrs_obj = models.ProductAttribute(**product_attributes_attrs)
    prod_attrs_exists = check_if_exists(models.ProductAttribute, raw_product_id_filter_query)
    if prod_attrs_exists:
        # update
        product_attrs_obj = get_obj(models.ProductAttribute, raw_product_id_filter_query)
        product_attrs_obj.product_type = product_type
        product_attrs_obj.size = size
        product_attrs_obj.material = material
        product_attrs_obj.color = color
        product_attrs_obj.gender = gender
        product_attrs_obj.age = age
        product_attrs_obj.date_updated = NOW()

    _add(product_attrs_obj)

    product_description = product_attrs['description']
    short_desc = product_description.get('short', None)
    long_desc = product_description.get('long', None)
    primary_category_id = product_attrs['category'].get('primary', None)
    secondary_category_id = product_attrs['category'].get('secondary', None)

    product_description_attrs = dict(raw_product_id=raw_product_id,
                                     primary_category_id=primary_category_id,
                                     secondary_category_id=secondary_category_id,
                                     description=short_desc, description_long=long_desc)
    product_description_obj = models.ProductDescription(**product_description_attrs)
    prod_desc_exists = check_if_exists(models.ProductDescription, raw_product_id_filter_query)
    if prod_desc_exists:
        # update
        product_description_obj = get_obj(models.ProductDescription, raw_product_id_filter_query)
        product_description_obj.short_desc = short_desc
        product_description_obj.long_desc = long_desc
        product_description_obj.primary_category_id = primary_category_id
        product_description_obj.secondary_category_id = secondary_category_id
        product_description_obj.date_updated = NOW()

    _add(product_description_obj)

    sku_id = product_attrs['sku_number']
    name = product_attrs['product_name']
    source = product_attrs['source']
    price_details = product_attrs['price_details']
    retail_price = price_details.get('retail', None)
    sale_price = price_details.get('sale', None)
    currency = price_details.get('@currency', None)

    product = models.Product(raw_product_id=raw_product_id, sku_id=sku_id,
                             name=name, source=source, retail_price=retail_price,
                             sale_price=sale_price, currency=currency,
                             manufacturer_id=get_obj(models.Manufacturer, manufacturer_filter_query).id,
                             brand_id=get_obj(models.Brand, brand_filter_query).id,

                             shipping_id=get_obj(models.Shipping, raw_product_id_filter_query).id,
                             product_availability_id=get_obj(models.ProductAvailability, raw_product_id_filter_query).id,
                             product_description_id=get_obj(models.ProductDescription, raw_product_id_filter_query).id,
                             product_attributes_id=get_obj(models.ProductAttribute, raw_product_id_filter_query).id)
    prod_exist_filter_query = {'raw_product_id': raw_product_id, 'sku_id': sku_id}
    prod_exists = check_if_exists(models.Product, prod_exist_filter_query)
    if prod_exists:
        product = get_obj(models.Product, prod_exist_filter_query)
        product.name = name
        product.source = source
        product.retail_price = retail_price
        product.sale_price = sale_price
        product.currency = currency
        product.date_updated = NOW()

    _add(product)
